import { Story, Meta } from '@storybook/angular/types-6-0';
import { LavaBtnComponent } from './lava-btn.component';
import { FormsModule } from '@angular/forms';
import { moduleMetadata } from '@storybook/angular';
export default {
    title: 'lava/Button',
    component: LavaBtnComponent,
    // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
    argTypes: {
      backgroundColor: { control: 'color' },
    },
  } as Meta;
  
  // More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
  const Template: Story<LavaBtnComponent> = (args: LavaBtnComponent) => ({
    props: args,
  });
  
  export const Primary = Template.bind({});
  // More on args: https://storybook.js.org/docs/angular/writing-stories/args
  Primary.args = {
    primary: true,
    label: 'Button',
  };
  
  export const Secondary = Template.bind({});
  Secondary.args = {
    label: 'Button',
  };
  
  export const Large = Template.bind({});
  Large.args = {
    size: 'large',
    label: 'Button',
  };
  
  export const Small = Template.bind({});
  Small.args = {
    size: 'small',
    label: 'Button',
  };
  