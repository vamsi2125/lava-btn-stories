import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LavaBtnComponent } from './lava-btn.component';

describe('LavaBtnComponent', () => {
  let component: LavaBtnComponent;
  let fixture: ComponentFixture<LavaBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LavaBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LavaBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
