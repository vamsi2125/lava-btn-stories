import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-lava-btn',
  templateUrl: './lava-btn.component.html',
  styleUrls: ['./lava-btn.component.scss']
})
export class LavaBtnComponent implements OnInit {

  @Input()
  primary = false;
  @Input()
  size: 'small' | 'medium' | 'large' = 'medium';
  @Input()
  label = 'Button';
  constructor() { }

  ngOnInit(): void {
  }
  @Output()
  onClick = new EventEmitter<Event>();

  public get classes(): string[] {
    const mode = this.primary ? 'storybook-button--primary' : 'storybook-button--secondary';

    return ['storybook-button', `storybook-button--${this.size}`, mode];
  }
}
